//
// Created by Moritz Fink on 24.09.19.
//

#ifndef RL_CARRENTAL_POLICY_H
#define RL_CARRENTAL_POLICY_H

#include <fstream>
#include "Matrix.h"
#include "Probabilities.h"

class StateActionNotPossibleException : public std::exception
{
    [[nodiscard]] const char* what() const noexcept override
    {
        return "This action is not callable";
    }
};

class Policy
{
private:
    double _gamma;
    double _threshold = 1.0;
    int _maxCars;
    Matrix<int>* _policy;
    Matrix<double>* _stateValues;

public:
    Policy(int stateCount, int defaultAction, double gamma);
    ~Policy();

    void Iterate(const Probabilities& probabilities);
    void OutputAsCsv(const char* filePath);

private:
    void Evaluation(const Probabilities& probabilities);
    bool Improvement(const Probabilities& probabilities);
    double CalculateValue(const StateAction& stateAction, const Probabilities& probabilities);
};


#endif //RL_CARRENTAL_POLICY_H
