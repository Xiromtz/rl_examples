//
// Created by Moritz Fink on 29.09.19.
//

#ifndef RL_CARRENTAL_STATEACTION_H
#define RL_CARRENTAL_STATEACTION_H

#include "State.h"

struct StateAction
{
    State m_state;
    int m_action;

    StateAction(State state, int action) : m_state(state), m_action(action) {}

    bool operator==(const StateAction& other) const
    {
        return other.m_state == m_state && other.m_action == m_action;
    }
};

namespace std
{
    template<> struct hash<StateAction>
    {
        size_t operator()(const StateAction& stateAction) const noexcept
        {
            size_t res = hash<State>{}(stateAction.m_state);
            res = res * 31 + std::hash<int8_t>()(stateAction.m_action);
            return res;
        }
    };
}

#endif //RL_CARRENTAL_STATEACTION_H
