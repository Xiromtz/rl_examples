//
// Created by Moritz Fink on 24.09.19.
//

#include "Probabilities.h"

Probabilities::Probabilities(const int expectedRentalPlace1, const int expectedReturnPlace1,
                             const int expectedRentalPlace2, const int expectedReturnPlace2)
{
    std::pair expectedValues1(expectedRentalPlace1, expectedReturnPlace1);
    std::pair expectedValues2(expectedRentalPlace2, expectedReturnPlace2);

    poissonProbabilities = new double*[4];
    SetPoissonProbabilities(expectedValues1, expectedValues2);
    GenerateStates();

    for (int carsPlace1 = 0; carsPlace1 <= _maxCars; carsPlace1++)
    {
        for (int carsPlace2 = 0; carsPlace2 <= _maxCars; carsPlace2++)
        {
            for (int action = -5; action <= 5; action++)
            {
                State state(carsPlace1, carsPlace2);
                StateAction stateAction(state, action);
                std::vector<Probabilities::StateWithRewards> probabilities = CalculateAllProbabilities(stateAction);

                if (probabilities.empty())
                    continue;

                std::pair<StateAction, std::vector<StateWithRewards>> KeyValuePair(stateAction, probabilities);
                stateActionProbabilities.insert(KeyValuePair);
            }
        }
    }
}

void Probabilities::SetPoissonProbabilities(const std::pair<int, int>& expectedValues1,
                                            const std::pair<int, int>& expectedValues2)
{
    for (int i = 0; i < 4; i++)
    {
        poissonProbabilities[i] = new double[_maxCars+1];

        int expected = 0;

        switch (i)
        {
            case 0:
                expected = expectedValues1.first;
                break;
            case 1:
                expected = expectedValues1.second;
                break;
            case 2:
                expected = expectedValues2.first;
                break;
            case 3:
                expected = expectedValues2.second;
                break;
            default:
                expected = 0;
                break;
        }

        for (int u = 0; u <= _maxCars; u++)
        {
            poissonProbabilities[i][u] = CalculatePoissonProbability(u, expected);
        }
    }
}

double Probabilities::CalculatePoissonProbability(const int actual, const int expected) const
{
    double power = pow(expected, actual);
    unsigned long int fact = factorial(actual);
    double part1 = power / fact;
    double exponential = exp(-expected);
    double probability = part1 * exponential;
    return probability;
}

void Probabilities::GenerateStates()
{
    const int16_t stateCount = _maxCars * _maxCars;
    states = new State[stateCount];

    for (uint16_t state = 0; state < stateCount; state++)
    {
        int carsPlace1 = state / _maxCars;
        int carsPlace2 = state % _maxCars;

        states[state].CarsPlace1 = carsPlace1;
        states[state].CarsPlace2 = carsPlace2;
    }
}

std::vector<Probabilities::StateWithRewards> Probabilities::CalculateAllProbabilities(const StateAction& stateAction) const
{
    std::vector<StateWithRewards> statesWithRewards;

    int action = stateAction.m_action;
    State state = stateAction.m_state;

    int nextCarsPlace1 = std::min(state.CarsPlace1 - action, _maxCars-1);
    int nextCarsPlace2 = std::min(state.CarsPlace2 + action, _maxCars-1);

    if (nextCarsPlace1 < 0 || nextCarsPlace2 < 0)
        return statesWithRewards;

    State stateAfterAction(nextCarsPlace1, nextCarsPlace2);
    uint16_t stateCount = _maxCars * _maxCars;

    for (uint16_t stateIndex = 0; stateIndex < stateCount; stateIndex++)
    {
        StateWithRewards stateWithRewards;
        State stateToCalculate = states[stateIndex];

        std::vector<RewardProbability> stateRewards = CalculateTransitionProbabilities(stateAfterAction, stateToCalculate);

        if (stateRewards.empty())
            continue;

        statesWithRewards.emplace_back(StateWithRewards(stateToCalculate, stateRewards));
    }

    return statesWithRewards;
}

std::vector<Probabilities::RewardProbability> Probabilities::CalculateTransitionProbabilities(const State& state, const State& nextState) const
{
    int carsPlace1 = state.CarsPlace1;
    int carsPlace2 = state.CarsPlace2;

    int nextCarsPlace1 = nextState.CarsPlace1;
    int nextCarsPlace2 = nextState.CarsPlace2;

    std::vector<RewardProbability> probabilities;

    for (int carsRentedPlace1 = 0; carsRentedPlace1 < _maxCars; carsRentedPlace1++)
    {
        double probabilityCarsRentedPlace1 = poissonProbabilities[0][carsRentedPlace1];
        int actualCarsRentedPlace1 = std::min(carsPlace1, carsRentedPlace1);
        int carsAfterRentPlace1 = carsPlace1 - actualCarsRentedPlace1;

        for (int carsRentedPlace2 = 0; carsRentedPlace2 < _maxCars; carsRentedPlace2++)
        {
            double probabilityCarsRentedPlace2 = poissonProbabilities[2][carsRentedPlace2];
            int actualCarsRentedPlace2 = std::min(carsPlace2, carsRentedPlace2);
            int carsAfterRentPlace2 = carsPlace2 - actualCarsRentedPlace2;

            int reward = 10 * actualCarsRentedPlace1 + 10 * actualCarsRentedPlace2;

            for (int carsReturnedPlace1 = 0; carsReturnedPlace1 < _maxCars; carsReturnedPlace1++)
            {
                double probabilityCarsReturnedPlace1 = poissonProbabilities[1][carsReturnedPlace1];
                int carsAfterReturnPlace1 = std::min(carsAfterRentPlace1 + carsReturnedPlace1, _maxCars - 1);

                for (int carsReturnedPlace2 = 0; carsReturnedPlace2 < _maxCars; carsReturnedPlace2++)
                {
                    double probabilityCarsReturnedPlace2 = poissonProbabilities[2][carsReturnedPlace2];
                    int carsAfterReturnPlace2 = std::min(carsAfterRentPlace2 + carsReturnedPlace2, _maxCars - 1);

                    if (carsAfterReturnPlace1 != nextCarsPlace1 || carsAfterReturnPlace2 != nextCarsPlace2)
                        continue;

                    double probabilityNextState = probabilityCarsRentedPlace1 * probabilityCarsRentedPlace2
                                                * probabilityCarsReturnedPlace1 * probabilityCarsReturnedPlace2;


                    RewardProbability rewardProbability(reward, probabilityNextState);

                    bool rewardExists = false;
                    for (auto & probability : probabilities)
                    {
                        if (probability.first == rewardProbability.first)
                        {
                            probability.second += rewardProbability.second;
                            rewardExists = true;
                            break;
                        }
                    }

                    if (rewardExists)
                        continue;

                    probabilities.emplace_back(rewardProbability);
                }
            }
        }
    }

    return probabilities;
}

Probabilities::~Probabilities()
{
    for (int i = 0; i < 4; i++)
    {
        delete[] poissonProbabilities[i];
    }

    delete[] poissonProbabilities;
    delete[] states;
}


