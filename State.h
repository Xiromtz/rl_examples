//
// Created by Moritz Fink on 29.09.19.
//

#ifndef RL_CARRENTAL_STATE_H
#define RL_CARRENTAL_STATE_H

#include <cstdint>
#include <string>

struct State
{
    int CarsPlace1 = 0;
    int CarsPlace2 = 0;

    State() {}
    State(int carsPlace1, int carsPlace2) : CarsPlace1(carsPlace1), CarsPlace2(carsPlace2) {}

    bool operator==(const State& other) const
    {
        return CarsPlace1 == other.CarsPlace1 && CarsPlace2 == other.CarsPlace2;
    }
};

namespace std
{
    template<> struct hash<State>
    {
        size_t operator()(const State& state) const noexcept
        {
            size_t res = 17;
            res = res * 31 + std::hash<int>()(state.CarsPlace1);
            res = res * 31 + std::hash<int>()(state.CarsPlace2);
            return res;
        }
    };
}

#endif //RL_CARRENTAL_STATE_H
