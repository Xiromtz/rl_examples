//
// Created by Moritz Fink on 24.09.19.
//

#ifndef RL_CARRENTAL_PROBABILITIES_H
#define RL_CARRENTAL_PROBABILITIES_H

#include <cstdint>
#include <cmath>
#include <unordered_map>
#include <vector>
#include "StateAction.h"

class Probabilities
{
public:
    typedef std::pair<int16_t, double> RewardProbability;
    typedef std::pair<State, std::vector<RewardProbability>> StateWithRewards;
    std::unordered_map<StateAction, std::vector<StateWithRewards>> stateActionProbabilities;

    double** poissonProbabilities;

private:
    // rental place 1; return place 1; rental place 2; return place 2


    State* states;

    const int _maxCars = 20;

    [[nodiscard]] std::vector<StateWithRewards> CalculateAllProbabilities(const StateAction& stateAction) const;
    [[nodiscard]] std::vector<RewardProbability> CalculateTransitionProbabilities(const State& state, const State& nextState) const;

    void SetPoissonProbabilities(const std::pair<int, int>& expectedValues1, const std::pair<int, int>& expectedValues2);
    void GenerateStates();

    [[nodiscard]] double CalculatePoissonProbability(int actual, int expected) const;

    [[nodiscard]] unsigned long int factorial(int number) const
    {
        if ((number == 0) || (number == 1))
            return 1;
        else
            return number * factorial(number - 1);
    }

public:

    Probabilities(int expectedRentalPlace1, int expectedReturnPlace1, int expectedRentalPlace2, int expectedReturnPlace2);
    ~Probabilities();
};


#endif //RL_CARRENTAL_PROBABILITIES_H
