//
// Created by Moritz Fink on 24.09.19.
//

#ifndef RL_CARRENTAL_MATRIX_H
#define RL_CARRENTAL_MATRIX_H

template<class T>
class Matrix
{
private:
    int _col;
    int _row;
    T** _matrix;

public:


    Matrix(int row, int col) : _row(row), _col(col)
    {
        _matrix = new T*[row];

        for (int i = 0; i < row; i++)
        {
            _matrix[i] = new T[col];
        }
    }

    Matrix(const Matrix& matrix)
    {
        _col = matrix._col;
        _row = matrix._row;

        _matrix = new T*[_row];

        for (int i = 0; i < _row; i++)
        {
            _matrix[i] = new T[_col];
            for (int col = 0; col < _col; col++)
            {
                _matrix[i][col] = matrix._matrix[i][col];
            }
        }
    }

    ~Matrix()
    {
        for (int i = 0; i < _col; i++)
        {
            delete _matrix[i];
        }

        delete[] _matrix;
    }

    void Set(int row, int col, T val)
    {
        _matrix[row][col] = val;
    }

    T Get(int row, int col)
    {
        return _matrix[row][col];
    }
};


#endif //RL_CARRENTAL_MATRIX_H
