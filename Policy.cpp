//
// Created by Moritz Fink on 24.09.19.
//

#include <iostream>
#include "Policy.h"

Policy::Policy(int stateCount, int defaultAction, double gamma) : _maxCars(stateCount), _gamma(gamma)
{
    _policy = new Matrix<int>(stateCount, stateCount);
    _stateValues = new Matrix<double>(stateCount, stateCount);

    for (int row = 0; row < stateCount; row++)
    {
        for (int col = 0; col < stateCount; col++)
        {
            _policy->Set(row, col, defaultAction);
            _stateValues->Set(row, col, defaultAction);
        }
    }
}



void Policy::Iterate(const Probabilities &probabilities)
{
    bool foundOptimalPolicy = false;

    /*while (!foundOptimalPolicy)
    {*/
        Evaluation(probabilities);
        foundOptimalPolicy = Improvement(probabilities);
    //}
}

void Policy::Evaluation(const Probabilities& probabilities)
{
    double delta = 0;

    do
    {
        delta = 0;

        for (int carsPlace1 = 0; carsPlace1 < _maxCars; carsPlace1++)
        {
            for (int carsPlace2 = 0; carsPlace2 < _maxCars; carsPlace2++)
            {
                int action = _policy->Get(carsPlace1, carsPlace2);

                State state(carsPlace1, carsPlace2);
                StateAction stateAction(state, action);

                double updated_stateValue = 0;

                try
                {
                    updated_stateValue = CalculateValue(stateAction, probabilities);
                }
                catch (StateActionNotPossibleException& e)
                {
                    continue;
                }

                double old_stateValue = _stateValues->Get(carsPlace1, carsPlace2);
                _stateValues->Set(carsPlace1, carsPlace2, updated_stateValue);

                delta = fmax(delta, abs(old_stateValue - updated_stateValue));
            }
        }
    } while (delta >= _threshold);
}

bool Policy::Improvement(const Probabilities& probabilities)
{
    bool stable = true;

    for (int carsPlace1 = 0; carsPlace1 < _maxCars; carsPlace1++)
    {
        for (int carsPlace2 = 0; carsPlace2 < _maxCars; carsPlace2++)
        {
            State state(carsPlace1, carsPlace2);
            int oldAction = _policy->Get(carsPlace1, carsPlace2);

            double maxValue = -HUGE_VAL;
            int maxAction = 0;

            for (int action = -5; action <= 5; action++)
            {
                StateAction stateAction(state, action);

                double value = 0;

                try
                {
                    value = CalculateValue(stateAction, probabilities);
                }
                catch (StateActionNotPossibleException& e)
                {
                    continue;
                }

                if (value > maxValue)
                {
                    maxValue = value;
                    maxAction = action;
                }
            }

            _policy->Set(carsPlace1, carsPlace2, maxAction);

            if (maxAction != oldAction)
                stable = false;
        }
    }

    return stable;
}

double Policy::CalculateValue(const StateAction& stateAction, const Probabilities& probabilities)
{
    double** poissonProbabilities = probabilities.poissonProbabilities;
    State state = stateAction.m_state;
    int action = stateAction.m_action;

    if (state.CarsPlace1 - action < 0 || state.CarsPlace2 + action < 0)
        throw StateActionNotPossibleException();

    double expectedValue = abs(action) * -2;

    if (action >= 1)
        expectedValue += 2;

    State stateAfterAction(std::min(state.CarsPlace1 - action, _maxCars-1), std::min(state.CarsPlace2 + action, _maxCars-1));

    if (stateAfterAction.CarsPlace1 > 10)
        expectedValue -= 4;

    if (stateAfterAction.CarsPlace2 > 10)
        expectedValue -= 4;

    double rewardBeforeRentAndReturn = expectedValue;

    for (int rentedPlace1 = 0; rentedPlace1 < _maxCars; rentedPlace1++)
    {
        double probabilityRentedPlace1 = poissonProbabilities[0][rentedPlace1];
        int actualRentedPlace1 = std::min(rentedPlace1, stateAfterAction.CarsPlace1);
        int carsAfterRentPlace1 = stateAfterAction.CarsPlace1 - actualRentedPlace1;

        for (int rentedPlace2 = 0; rentedPlace2 < _maxCars; rentedPlace2++)
        {
            double probabilityRentedPlace2 = poissonProbabilities[2][rentedPlace2];
            int actualRentedPlace2 = std::min(rentedPlace2, stateAfterAction.CarsPlace2);
            int carsAfterRentPlace2 = stateAfterAction.CarsPlace2 - actualRentedPlace2;

            double probabilityRented = probabilityRentedPlace1 * probabilityRentedPlace2;

            int reward = (actualRentedPlace1 + actualRentedPlace2) * 10;

            for (int returnedPlace1 = 0; returnedPlace1 < _maxCars; returnedPlace1++)
            {
                double probabilityReturnedPlace1 = poissonProbabilities[1][returnedPlace1];
                int carsAfterReturnPlace1 = std::min(carsAfterRentPlace1 + returnedPlace1, _maxCars-1);

                for (int returnedPlace2 = 0; returnedPlace2 < _maxCars; returnedPlace2++)
                {
                    double probabilityReturnedPlace2 = poissonProbabilities[3][returnedPlace2];
                    int carsAfterReturnPlace2 = std::min(carsAfterRentPlace2 + returnedPlace2, _maxCars-1);

                    double probabilityReturned = probabilityReturnedPlace2 * probabilityReturnedPlace1;

                    double probability = probabilityReturned * probabilityRented;
                    State next(carsAfterReturnPlace1, carsAfterReturnPlace2);

                    expectedValue += probability * (reward + 0.9 * _stateValues->Get(next.CarsPlace1, next.CarsPlace2));
                }
            }
        }
    }

    auto stateActionIterator = probabilities.stateActionProbabilities.find(stateAction);
    if (stateActionIterator == probabilities.stateActionProbabilities.end())
        throw StateActionNotPossibleException();

    double updated_stateValue = 0;

    std::vector<Probabilities::StateWithRewards> stateRewards = stateActionIterator->second;
    for (auto & stateReward : stateRewards)
    {
        const State nextState = stateReward.first;
        const double nextStateValue = _stateValues->Get(nextState.CarsPlace1,nextState.CarsPlace2);

        std::vector<Probabilities::RewardProbability> rewardProbabilities = stateReward.second;
        for (auto & rewardProbability : rewardProbabilities)
        {
            double part_stateValue = rewardProbability.second * (rewardProbability.first + _gamma * nextStateValue);
            updated_stateValue += part_stateValue;
        }
    }

    std::cout << "Wrong Calculated Value: " << (updated_stateValue + rewardBeforeRentAndReturn) << std::endl;
    std::cout << "Correct CalculatedValue: " << expectedValue << std::endl << std::endl;

    return expectedValue;
}

void Policy::OutputAsCsv(const char* const filePath)
{
    std::ofstream csv;
    csv.open(filePath);

    for (int row = _maxCars - 1; row >= 0; row--)
    {
        for (int col = 0; col < _maxCars; col++)
        {
            csv << _policy->Get(row, col) << ";";
        }
        csv << "\n";
    }

    csv.close();
}

Policy::~Policy()
{
    delete _policy;
    delete _stateValues;
}



