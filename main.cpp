#include "Policy.h"

int main(int argc, char **argv)
{
    /*
     * Arguments:
     * rent + return in place1 + place2 = 4 arguments
     * discount rate gamma - [0,1]
     */

    if (argc <= 5)
        return 0;

    std::string expectedRentPlace1String = argv[1];
    std::string expectedReturnPlace1String = argv[2];
    std::string expectedRentPlace2String = argv[3];
    std::string expectedReturnPlace2String = argv[4];

    std::string discountString = argv[5];

    int expectedRentPlace1 = std::stoi(expectedRentPlace1String);
    int expectedReturnPlace1 = std::stoi(expectedReturnPlace1String);
    int expectedRentPlace2 = std::stoi(expectedRentPlace2String);
    int expectedReturnPlace2 = std::stoi(expectedReturnPlace2String);
    float discount = std::stof(discountString);

    Probabilities probabilities(expectedRentPlace1, expectedReturnPlace1, expectedRentPlace2, expectedReturnPlace2);
    Policy policy(21, 0, discount);
    policy.Iterate(probabilities);
    policy.OutputAsCsv("./emptyPolicy.csv");

    return 0;
}